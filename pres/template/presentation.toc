\beamer@sectionintoc {1}{Architectures}{3}{0}{1}
\beamer@sectionintoc {2}{Problem: Long term dependencies}{5}{0}{2}
\beamer@sectionintoc {3}{Propopsed solutions}{9}{0}{3}
\beamer@sectionintoc {4}{LSTMs}{11}{0}{4}
\beamer@sectionintoc {5}{Optimization for Long-term dependencies}{18}{0}{5}
\beamer@sectionintoc {6}{Explicit Memory}{20}{0}{6}
